#!/usr/bin/env python
# © 2015 Trust+ <https://trust.plus/>  MIT License
import json, requests, requests.auth

class Client(object):
  def __init__(self, api_key='', endpoint='https://api.trust.plus/check', verify_ssl=True):
    self.api_key = api_key
    self.endpoint = endpoint
    self.verify_ssl = verify_ssl

  def check(self, **kwargs):
    return self.__callMethod('trustplus.Check', kwargs)

  def __callMethod(self, method, args):
    res = requests.post(self.endpoint,
      data=json.dumps(dict(
        method=method,
        params=args,
        id=1,
        jsonrpc="2.0"
      )),
      headers={
        "Content-Type": "application/json-rpc",
        "User-Agent": "trustplus-python/1.0",
      },
      verify=self.verify_ssl,
      auth=requests.auth.HTTPBasicAuth(self.api_key, '')
    ).json()

    return res['result']
