#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
  name='trustplus',
  version='1.0.0',
  description='Python binding for the Trust+ API',
  url='https://trust.plus/',
  license='MIT',
  classifiers=[
    'Programming Language :: Python :: 2',
    'Programming Language :: Python :: 3',
  ],
  keywords='api',
  author='Trust+',
  author_email='support@trust.plus',
  packages=find_packages(),
  install_requires=[],
  extras_require={},
  entry_points={},
)
